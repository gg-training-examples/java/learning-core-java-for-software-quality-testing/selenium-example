package com.groundgurus.selenium_example;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		// set the downloaded driver in the properties
		String driverPath = System.getProperty("user.home") 
				+ File.separator + "chromedriver";
		System.setProperty("webdriver.chrome.driver", driverPath);
		
		String reportPath = System.getProperty("user.home")
				+ File.separator + "Documents" + File.separator + "myreport.html";
		ExtentReports reports = new ExtentReports(reportPath);
		
		ExtentTest sampleTest = reports.startTest("sampletest");
		
		// load driver
	    WebDriver driver = new ChromeDriver();
	    
	    driver.get("http://thedemosite.co.uk/savedata.php");
	    sampleTest.log(LogStatus.INFO, "Website was loaded");
	    
	    WebElement username = driver.findElement(By.name("username"));
	    username.sendKeys("myuser123");
	    sampleTest.log(LogStatus.INFO, "Entered username");
	    
	    WebElement password = driver.findElement(By.name("password"));
	    password.sendKeys("mypass123");
	    sampleTest.log(LogStatus.INFO, "Entered password");
	    
	    WebElement save = driver.findElement(By.name("FormsButton2"));
	    save.click();
	    sampleTest.log(LogStatus.INFO, "Clicked on save");
	    
	    WebElement result = driver.findElement(By.xpath("/html/body/table/tbody/tr/td[1]/blockquote/blockquote[2]/blockquote"));
	    System.out.println(result.getText());
	    sampleTest.log(LogStatus.INFO, "Retrieved result");
	    
	    // end the sampleTest
	    reports.endTest(sampleTest);
	    
	    // write the markups
	    reports.flush();
	    
	    reports.close();

	    driver.close();
	}
}
