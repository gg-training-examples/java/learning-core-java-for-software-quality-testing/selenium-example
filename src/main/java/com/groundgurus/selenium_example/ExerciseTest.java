package com.groundgurus.selenium_example;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class ExerciseTest {
	public static void main(String[] args) {
		// set the downloaded driver in the properties
		String driverPath = System.getProperty("user.home") + File.separator + "chromedriver";
		System.setProperty("webdriver.chrome.driver", driverPath);

		String reportPath = System.getProperty("user.home") + File.separator + "Documents" + File.separator
				+ "imdbreport.html";
		ExtentReports reports = new ExtentReports(reportPath);

		ExtentTest sampleTest = reports.startTest("sampletest");

		// load driver
		WebDriver driver = new ChromeDriver();

		driver.get("https://www.imdb.com/search/");
		sampleTest.log(LogStatus.INFO, "Website was loaded");

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		WebElement query = driver.findElement(By.name("q"));
		query.click();
		query.sendKeys("Avengers End Game");
		
		sampleTest.log(LogStatus.INFO, "Entered a query");

		query.submit();
		sampleTest.log(LogStatus.INFO, "Submitted the form");

		WebElement result = driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[2]/table/tbody/tr[1]/td[2]"));
		System.out.println(result.getText());
		
		// end the sampleTest
		reports.endTest(sampleTest);

		// write the markups
		reports.flush();

		reports.close();

		driver.close();
	}
}
